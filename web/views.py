from django.shortcuts import render
from django.http import HttpResponse
 
# Create your views here.
def index(request):
    return render(request, 'Home.html')

# Create your views here.
def index_2(request):
    return render(request, 'Intro.html')

# Create your views here.
def index_3(request):
    return render(request, 'Skills.html')

# Create your views here.
def index_4(request):
    return render(request, 'Activity.html')

# Create your views here.
def index_5(request):
    return render(request, 'Achieve.html')

# Create your views here.
def index_6(request):
    return render(request, 'Contact.html')

# Create your views here.
def index_7(request):
    return render(request, 'Question.html')
